# Hisilicon Hi35xx sample Makefile

export CUR_PATH?=$(shell pwd)
TOP_DIR=$(shell pwd)
BUILD=$(TOP_DIR)/build
SRC_ROOT = .

CFLAGS_COM = -DLINUX -Os -D_OS_LINUX -fPIC -Wno-psabi -mfpu=neon-vfpv4 -mcpu=cortex-a7 -mfloat-abi=hard
export CFLAGS_COM

CFLAGS += $(CFLAGS_COM)

CFLAGS += -I./                                 	\
		  -I./include						   	\
          -I./include/ni_common/           	   	\
          -I./src/py_intf                      	\
		  -I./include/eigen            			\
          -I./include/libwebsockets    			\
          -I./include/json-c           			\
          -I./include/network          			\
          -I./include/jerryscript      			\
          -I./include/zbar             			\
		  -I./include/tuxeip           			\
		  -I./include/nodave           			\
          -I./include/curl             			\
          -I./include/tensorflow       			\
          -I./include/micropython      			\
          -I./include/modbus           			\
          -I./include/opencv           			\
          -I./include/mlx90640         			\
		  -I./include/algs					    \

CPPFLAGS += -std=c++11 -Os -DCHIP_VER=RV1106

SENSOR_LIBS =   \
				-Xlinker "-("  \
			   ./libs/libmpcore.a              \
			   ./libs/libvcore.a               \
			   ./libs/libsoc_rv1106.a          \
			   ./libs/libvcomm.a               \
               ./libs/libmicropython.a         \
			   ./libs/libwebsockets.a          \
               ./libs/libnetwork.a             \
               ./libs/libzbar.a                \
               ./libs/libmlx90640.a            \
				-Xlinker "-)"\

#CC      = $(Q)$(CROSS_COMPILE)gcc
#CXX     = $(Q)$(CROSS_COMPILE)g++
#AS      = $(Q)$(CROSS_COMPILE)as
#LD      = $(Q)$(CROSS_COMPILE)ld
#AR      = $(Q)$(CROSS_COMPILE)ar
#RM      = $(Q)rm
#CPP     = $(Q)$(CROSS_COMPILE)cpp
#SIZE    = $(Q)$(CROSS_COMPILE)size
#STRIP   = $(Q)$(CROSS_COMPILE)strip -s
#OBJCOPY = $(Q)$(CROSS_COMPILE)objcopy
#OBJDUMP = $(Q)$(CROSS_COMPILE)objdump
MKDIR   = $(Q)mkdir
ECHO    = $(Q)@echo
MAKE    = $(Q)make
CAT     = $(Q)cat

TARGET := socket_httpd

TARGET_PATH := $(PWD)

MICROPY_DIR=micropython

CFLAGS += -L./libs/
CFLAGS += -DMICROPY_PY_THREAD=1
CFLAGS += -DMICROPY_PY_PYB_LEGACY=1

LIBS := -ldl -lpthread -lm -lz \
	   	-lssl \
		-lcrypto \
		-ljson-c \
		-lffi \
		-lsvm \
		-lcurl \
		-ljerry-core \
		-ljerry-ext \
		-ljerry-port-default \
		-lrkaiq \
		-ldbus-1 \
		-lrknn_api \
		-liconv \
		-lrockchip_mpp \
		-lasound \
		-lrga \
		-lrockit \
		-lrve \
		-lpcre \
		-lopencv_core \
		-lopencv_features2d \
		-lopencv_flann \
		-lopencv_imgproc \


MP_LIBS += -ldl -lpthread -lm

OMV_QSTR_DEFS = $(TOP_DIR)/src/py_intf/qstrdefsomv.h $(TOP_DIR)/src/py_intf/pins_qstr.h

MP_CFLAGS += -I$(BUILD)/$(MICROPY_DIR)/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/py/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/lib/mp-readline
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/lib/oofatfs
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/usbdev/core/inc/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/usbdev/class/inc/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/variants/standard/
MP_CFLAGS += -I$(MP_BOARD_CONFIG_DIR)

MICROPY_ARGS = BOARD=$(TARGET) DEBUG=$(DEBUG) QSTR_DEFS="$(OMV_QSTR_DEFS)"

ifeq ($(MICROPY_PY_IMU), 1)
#MP_CFLAGS += -DMICROPY_PY_IMU=1
#MICROPY_ARGS += MICROPY_PY_IMU=1
endif
ifeq ($(MICROPY_PY_ULAB), 1)
#MP_CFLAGS += -DMICROPY_PY_ULAB=1
#MICROPY_ARGS += MICROPY_PY_ULAB=1
endif

MP_CFLAGS += -DMICROPY_PY_IMAGE=1
MICROPY_ARGS += MICROPY_PY_IMAGE=1
HEADER_BUILD = $(BUILD)/micropython/genhdr

CFLAGS += $(ST_CFLAGS) $(MP_CFLAGS) $(OMV_CFLAGS)

SRC += $(addprefix ,    \
	$(wildcard src/*.cpp) \
	)

MSRCS += $(addprefix ,    \
	$(wildcard py_intf/*.c) 			\
	)

OBJS  = $(addprefix $(BUILD)/, $(MSRCS:.c=.o))
OBJS += $(addprefix $(BUILD)/, $(SRC:.cpp=.o))

OBJ_DIRS = $(sort $(dir $(OBJS)))

BOBJS = objs

MAP := $(TARGET).map

.PHONY : clean all

all: $(TARGET)

$(BUILD):
	$(MKDIR) -p $@

$(BUILD)/%.o : %.cpp
	$(CXX) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

$(BUILD)/%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(BUILD)/%.o : %.s
	$(ECHO) "AS $<"
	$(AS) $(AFLAGS) $< -o $@

$(HEADER_BUILD):
	$(MKDIR) -p $@

$(OBJ_DIRS):
	$(MKDIR) -p $@

$(TARGET): $(OBJ_DIRS) $(BUILD) $(BOBJS) $(OBJS)
	$(CXX) -o $(TARGET)  $(CFLAGS) $(LIBS) $(OBJS) $(SENSOR_LIBS)

$(BOBJS):
	$(MAKE) deplibs V=1  -C $(TARGET_PATH)/../3dparty/source/$(MICROPY_DIR)/ports/unix  BUILD=$(BUILD)/$(MICROPY_DIR)  $(MICROPY_ARGS)
	$(MAKE) libs V=1  -C $(TARGET_PATH)/../3dparty/source/$(MICROPY_DIR)/ports/unix  BUILD=$(BUILD)/$(MICROPY_DIR)  $(MICROPY_ARGS)
#	$(MAKE) -C src/py_intf CHIP_VER=RV1106  BUILD=$(BUILD) -f Makefile_py

clean:
	@rm -f $(OBJS)
	@rm -rf $(BUILD)

