# This file is part of the OpenMV project.
#
# Copyright (c) 2013-2019 Ibrahim Abdelkader <iabdalkader@openmv.io>
# Copyright (c) 2013-2019 Kwabena W. Agyeman <kwagyeman@openmv.io>
#
# This work is licensed under the MIT license, see the file LICENSE for details.
#
# OMV Makefile

TOP_DIR ?=$(shell pwd)
BUILD ?=$(TOP_DIR)/build
SRC_ROOT ?= .

MKDIR   = mkdir

MICROPY_DIR=micropython
HEADER_BUILD = $(BUILD)/micropython/genhdr
MP_CFLAGS += -I$(BUILD)/$(MICROPY_DIR)/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/py/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/lib/mp-readline
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/lib/oofatfs
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/usbdev/core/inc/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/usbdev/class/inc/
MP_CFLAGS += -I$(TOP_DIR)/../3dparty/source/$(MICROPY_DIR)/ports/unix/variants/standard/
#MP_CFLAGS += -I$(MP_BOARD_CONFIG_DIR)

CFLAGS += -Wall -fPIC -Os
CFLAGS += $(CFLAGS_COM)

CFLAGS += -D_OS_LINUX -Wno-psabi
CFLAGS += -DUSE_PY

CFLAGS += -I./                         \
		  -I./include/algs             \
          -I./include                  \
		  -I./include/eigen            \
          -I./include/libwebsockets    \
          -I./include/json-c           \
          -I./include/network          \
          -I./include/jerryscript      \
          -I./include/zbar             \
		  -I./include/tuxeip           \
		  -I./include/nodave           \
          -I./include/curl             \
          -I./include/tensorflow       \
          -I./include/micropython      \
          -I./include/modbus           \
          -I./include/opencv           \
		  -I./include/mlx90640         \
		  -I./include/svm              \
		  -I./ni_common/               \
		  
CFLAGS += $(MP_CFLAGS)
CFLAGS += -I$(HEADER_BUILD)
CFLAGS += -DMICROPY_PY_THREAD=1
CFLAGS += -D$(CHIP_VER)=1
CFLAGS += -DMICROPY_PY_PYB_LEGACY=1

CPPFLAGS += -std=c++11 -Os -fPIC

SRCS += $(addprefix ,    \
	$(wildcard src/script/port_py/*.c) 	\
	)

SRCPPS = $(addprefix ,    \
	$(wildcard src/script/port_py/*.cpp)\
	)

OBJS = $(addprefix $(BUILD)/, $(SRCS:.c=.o))
OBJS += $(addprefix $(BUILD)/, $(SRCPPS:.cpp=.o))
OBJ_DIRS = $(sort $(dir $(OBJS)))

TARGET := libmpcore

all: $(TARGET)

$(TARGET): $(OBJ_DIRS) $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -shared -o $(BUILD)/$(TARGET).so
	$(AR) rcv $(BUILD)/$(TARGET).a $(OBJS)

$(OBJ_DIRS):
	$(MKDIR) -p $@

$(BUILD)/%.o : %.cpp
	$(CXX) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

$(BUILD)/%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<
#	$(ECHO) "CC $<"

$(BUILD)/%.o : %.s
	$(ECHO) "AS $<"
	$(AS) $(AFLAGS) $< -o $@

#-include $(OBJS:%.o=%.d)

